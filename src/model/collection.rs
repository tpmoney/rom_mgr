use std::{fmt::Display, path::PathBuf};

use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Collection {
    pub name: String,
    dat_files: Vec<PathBuf>,
    source_dirs: Vec<PathBuf>,
    output_dir: Option<PathBuf>,
}

impl Display for Collection {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.name)
    }
}

impl Collection {
    pub fn new() -> Self {
        Collection {
            name: "Unnamed".to_owned(),
            dat_files: Vec::new(),
            source_dirs: Vec::new(),
            output_dir: Option::None,
        }
    }
}
