use serde::{Deserialize, Serialize};

use super::collection::Collection;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct NonMameManager {
    pub collections: Vec<Collection>,
}
